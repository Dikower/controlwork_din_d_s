#include <iostream>
#include <vector>

class StackA {
public:
  StackA() = default;

  explicit StackA(int m_length) {
    max_length = m_length;
    data = new double [max_length];
  }

  ~StackA() {
    delete[] data;
  }

  void Put(double el) {
    if (last < max_length) {
      last++;
      data[last] = el;
    }
  }

  double Get() {
    if (last > -1) {
      double res = data[last];
      last--;
      return res;
    }
    return 0.0;
  }

private:
  double *data = nullptr;
  int max_length{0};
  int last{-1};
};

int main() {
  StackA stack(2);
  stack.Put(2.2);
  stack.Put(4.5);
  std::cout << stack.Get();
  return 0;
}
